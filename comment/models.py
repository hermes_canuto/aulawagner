from django.db import models

from post.models import Post


# Create your models here.

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name="Post",
                             related_name="comment_post")
    comment = models.TextField(verbose_name='Post')
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    like = models.BooleanField(verbose_name="Like", default=1)

    def __str__(self):
        return self.post
