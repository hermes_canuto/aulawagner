from django.shortcuts import render, HttpResponse
from post.models import Post


# Create your views here.


def home(request):
    post_list = Post.objects.all()
    context = {
        'postlist': post_list
    }
    template_name = 'home.html'
    return render(request, template_name, context)


def post(request, slug):
    return HttpResponse(slug)
