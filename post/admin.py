from django.contrib import admin
from post.models import Post
from datetime import date


# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_filter = ['active']
    start_date = date.today()
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Post, PostAdmin)
