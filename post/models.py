from django.db import models
# from comment.models import Comment


# Create your models here.


class Post(models.Model):
    title = models.CharField(verbose_name='Titulo', max_length=100)
    slug = models.SlugField('Slug')
    description = models.TextField(verbose_name='Post')
    start_date = models.DateField('Data de Inicio',
                                  null=True,
                                  blank=True)

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)
    active = models.BooleanField(verbose_name='Publicar', default=True)

    class Meta:
        ordering = ['title', 'created_at']

    def __str__(self):
        return self.title

